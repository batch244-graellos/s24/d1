// ES6 Updates 
// ES - ECMAScript - European Computer Manufacturers Association Script

// [SECTION] Exponent Operator

// Using the exponent operator
const firstNum = 8 ** 2;
console.log(firstNum);

// Using Math object methods
const secondNum = Math.pow(8, 2);
console.log(secondNum);

// [SECTION] Template Literals

// Pre-Template Literals
// Uses single/double quotes and + for concatenation
let name = "John";
let message = "Hello " + name + "! Welcome to programming!";
console.log("Message without template literals: " + message);

// Strings using Template Literals
// Uses backticks (``) instead of ("") or ('')
message = `Hello ${name}! Welcome to programming!`;
console.log(`Message with template literals: ${message}`)

// Multi-line using template literals
const anotherMessage = `${name} attended a math competition.
He won it by solving the problem 8 ** 2 with the solution of ${firstNum}.`;
console.log(anotherMessage);

// [SECTION] Array Destructuring
/*
	- Allows us to unpack elements inside arrays into distinct variables.
	- Allows us to name array elements with variables instead of using index numbers.
*/

const fullName = ["Juan", "Dela", "Cruz"];

console.log(fullName[0]);
console.log(fullName[1]);
console.log(fullName[2]);

// Hello Juan Dela Cruz! It's nice to meet you!
console.log("Hello " + fullName[0] + fullName[1]  + fullName[2] + "! It's nice to meet you!" );
console.log(`Hello ${fullName[0]} ${fullName[1]} ${fullName[2]}! It's nice to meet you!`);

// Array Destructuring
// Variables for array destructuring is based on the developer's choice
const [firstName, middleName, lastName] = fullName;

console.log(firstName);
console.log(middleName);
console.log(lastName);

console.log(`Hello ${firstName} ${middleName} ${lastName}! It's nice to meet you!`);

function getFullName([firstName, middleName, lastName]) {
	console.log(`${firstName} ${middleName}, ${lastName}`);
}
getFullName(fullName);


// [SECTION] Object Desctructuring

const person = {
	givenName: "Jane",
	midName: "Dela",
	familyName: "Cruz"
};

// Pre-Object Desctructuring
console.log(person.givenName);
console.log(person.midName);
console.log(person.familyName);

console.log(`Hello ${person.givenName} ${person.midName} ${person.familyName}! It's good to see you again.`)

// Object Desctructuring
const {givenName, midName, familyName} = person;
console.log(givenName);
console.log(midName);
console.log(familyName);

console.log(`Hello ${givenName} ${midName} ${familyName}! It's good to see you again.`)

function displayFullName({givenName, midName, familyName}) {
	console.log(`${givenName} ${midName}, ${familyName}`);
}
displayFullName(person);

// [SECTION] Arrow function

function greeting(){
	console.log("Hi, Batch 244!");
}
greeting()

// Pre-arrow function
// function printFullName(firstName, middleName, lastName) {
// 	console.log(firstName + " " + middleName + " " + lastName);
// }
// printFullName("John", "D", "Smith");

const printFullName = (firstName, middleName, lastName) => {
	console.log(`${firstName} ${middleName} ${lastName}`)
}
printFullName("John", "D", "Smith");